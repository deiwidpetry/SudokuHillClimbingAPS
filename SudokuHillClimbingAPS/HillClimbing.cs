﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SudokuHillClimbing
{
    public class HillClimbing : Solver
    {
        private int[,] estadoAtual = new int[9, 9];
        private int[,] estadoDaSolucao;


        private int[,] melhorEstado;
        private int countErrados = 250;

        private int interacoes = 0;

        public HillClimbing()
        {

        }

        //Este método toma um sudoku como entrada e saídas
        //a solução do sudoku.Este método deve ser o único
        //método que pode ser chamado por um programa usando esta classe.
        //Ele vai cuidar de tudo.
        public Resultado solucionar(int[,] sudoku)
        {
            //temp = new Sudoku(sudoku.pegarEstadoInicial());
            //Faz uma cópia de sudoku para trabalhar.
            Resultado oResultado;
            temp = new Sudoku(sudoku);

            // Copie o estado inicial do sudoku para classe Hill climbing.
            setarEstadoInicial(temp);

            //Copie o estado inicial para o estado atual.
            for (int i = 0; i < 9; i++)
                for (int j = 0; j < 9; j++)
                    estadoAtual[i, j] = estadoInicial[i, j];

            // Preencha os espaços em branco com um conjunto aleatório de números.
            inicializarEstado(estadoAtual);
            temp.setarEstadoAtual(estadoAtual);
            if (temp.verifique() != 0)
            {
                do
                {
                    // Isso prepara o palco para a recursão.  interacoes é usado para limitar o número de vezes que a subida é chamada para evitar um loop infinito.
                    interacoes = 0;

                    // Preencha os espaços em branco com um conjunto aleatório de números.
                    inicializarEstado(estadoAtual);

                    estadoDaSolucao = climb(estadoAtual);

                    temp.setarEstadoAtual(estadoDaSolucao);
                } while (temp.verifique() != 0 && interacoes <200);

                Console.WriteLine(countErrados);
                //return estadoDaSolucao;
                oResultado = new Resultado(countErrados, melhorEstado);
                return oResultado;
            }
            else
            {
                oResultado = new Resultado(countErrados, estadoAtual);
                return oResultado;
            }
            

        }

        private int[,] climb(int[,] estado)
        {

            //Printar(estado);
            try
            {
                Random random = new Random();
                int valorTemporario, indiceDaLinhaRandom = 0;
                int[] colIndex = new int[2];
                int contErrosVizinho;
                int contErrosAtual;
                bool valido = false;

                //while (!valido)
                //{
                //    //Escolhe uma linha aleatória no Estado inicial, e se dois elementos aleatórios que pertencem nessa linha ambos iguais a zero, troque esses valores no Estado vizinho.
                //    indiceDaLinhaRandom = random.Next(9);
                //    valido = linha(estado, indiceDaLinhaRandom).Contains(0);
                //}

                //Escolhe uma linha aleatória no Estado inicial, e se dois elementos aleatórios que pertencem nessa linha ambos iguais a zero, troque esses valores no Estado vizinho.
                indiceDaLinhaRandom = random.Next(9);

                do
                {
                    for (int i = 0; i < 2; i++)
                        colIndex[i] = random.Next(9);
                } while (estadoInicial[indiceDaLinhaRandom, colIndex[0]] != 0
                        || estadoInicial[indiceDaLinhaRandom, colIndex[1]] != 0);

                valorTemporario = estado[indiceDaLinhaRandom, colIndex[0]];
                estado[indiceDaLinhaRandom, colIndex[0]] =
                        estado[indiceDaLinhaRandom, colIndex[1]];
                estado[indiceDaLinhaRandom, colIndex[1]] = valorTemporario;

                temp.setarEstado(estado);
                contErrosVizinho = temp.verifique();

                temp.setarEstadoAtual(estadoAtual);
                contErrosAtual = temp.verifique();
               
                //Incrementar interacoes para acompanhar quantas vezes a escalada foi chamada.
                //Se foi chamado 200 vezes, então precisamos reiniciar todo o processo.
                interacoes++;
                if (interacoes == 200)
                {
                    //for (int i = 0; i < 9; i++)
                    //{
                    //    for (int j = 0; j < 9; j++)
                    //    {
                    //        Console.WriteLine(estadoAtual[i, j]);
                    //    }
                    //}
                    return estadoAtual;
                }

                if (countErrados > contErrosVizinho)
                {
                    countErrados = contErrosVizinho;
                    melhorEstado = estado;
                }

                if (contErrosVizinho == 0)
                {
                    //Printar(estado);
                    return estado;
                }
                else if (contErrosVizinho >= contErrosAtual)
                {
                    //Printar(estadoAtual);
                    return climb(estadoAtual);
                }
                else
                {
                    //Printar(estado);
                    return climb(estado);
                }
            }
            catch (Exception)
            {
                return climb(estadoAtual);
            }
        }

        private void Printar(int[,] quadro)
        {
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    Console.Write(string.Format("{0} ", quadro[i, j]));
                }
                Console.Write(Environment.NewLine);
            }
        }


    }
}
