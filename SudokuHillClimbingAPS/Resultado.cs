﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SudokuHillClimbing
{
    public class Resultado
    {
        public int countErrados { get; set; }
        public int[,] melhorEstado { get; set; }

        public Resultado(int count, int[,] estado)
        {
            countErrados = count;
            melhorEstado = estado;
        }
        public Resultado()
        {
        }
    }
}
