﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SudokuHillClimbing
{
	public class Sudoku
	{
		//Matrizes para armazenar cada caixa do sudoku para fins de verificação.
		int[,] tempUm = new int[3, 3];
		int[,] tempDois = new int[3, 3];
		int[,] tempTres = new int[3, 3];
		int[,] tempQuatro = new int[3, 3];
		int[,] tempCinco = new int[3, 3];
		int[,] tempSeis = new int[3, 3];
		int[,] tempSete = new int[3, 3];
		int[,] tempOito = new int[3, 3];
		int[,] tempNove = new int[3, 3];
		//O estado inicial do quebra-cabeça.Este é o começo do quebra-cabeça.Zeros representam espaços, caso contrário cada elemento na matriz deve ser números entre 1 -9.
		private int[,] estadoInicial = new int[9,9];

		//O estado atual do quebra-cabeça.Esta é a resposta do usuário.Zeros representam espaços, caso contrário cada elemento na matriz deve ser números entre 1 -9.
		private int[,] estadoAtual = new int[9,9];

		private int[,] estado = new int[9, 9];

		//Construtor sem argumentos.Isso cria um tabuleiro vazio.Isso é útil no caso de o usuário querer entrar em um Sudoku completo 
		//para simplesmente verificar se sua resposta é correto.
		public Sudoku()
		{
			for (int i = 0; i < 9; i++)
				for (int j = 0; j < 9; j++)
					estadoInicial[i,j] = estadoAtual[i,j] = 0;
		}

		// Construtor que toma uma matriz bi dimensional como argumento.  Este construtor pega a matriz e se copia em ambas as matrizes Sudoku.
		//Nota: Precisa de verificação de erros para verificar esse estado matriz é uma matriz 9x9.
		public Sudoku(int[,] estado)
		{
			for (int i = 0; i < 9; i++)
				for (int j = 0; j < 9; j++)
					estadoInicial[i,j] = estadoAtual[i,j] = estado[i,j];
		}

		//Isso imprime o estado atual para o console.
		//Para propósitos de depuração.
		public void printEstadoAtual()
		{
			for (int i = 0; i < 9; i++)
			{
				for (int j = 0; j < 9; j++)
				{
					Console.WriteLine(estadoAtual[i,j]);
				}
				Console.WriteLine("");
			}
		}

		//Isso imprime o estado inicial para o console.
		//Para propósitos de depuração.
		public void printEstadoInicial()
		{
			for (int i = 0; i < 9; i++)
			{
				for (int j = 0; j < 9; j++)
				{
					Console.WriteLine(estadoInicial[i,j]);
				}
				Console.WriteLine("");
			}
		}

		private int[,] copiarEstado(int[,] estadoASerCopiado)
		{
			int[,] estadoTemp = new int[9,9];

			for (int i = 0; i < 9; i++)
				for (int j = 0; j < 9; j++)
					estadoTemp[i,j] = estadoASerCopiado[i,j];

			return estadoTemp;
		}

		public int[,] pegarEstadoAtual()
		{
			return copiarEstado(estadoAtual);
		}

		public int[,] pegarEstadoInicial()
		{
			return copiarEstado(estadoInicial);
		}

		//Isso define o estado atual para qualquer coisa que o usuário quer.
		//Note que o estado inicial não vai ter uma função como esta para que o programa possa verificar que o
		//quadrados no estado atual que são preenchido no estado inicial não são Substituído.
		public int setarEstadoAtual(int[,] estado)
		{
			for (int i = 0; i < 9; i++)
				for (int j = 0; j < 9; j++)
					estadoAtual[i,j] = estado[i,j];

			return 0;
		}

		public int setarEstado(int[,] estado)
		{
			for (int i = 0; i < 9; i++)
				for (int j = 0; j < 9; j++)
					this.estado[i, j] = estado[i, j];

			return 0;
		}

		//Esta função verifica a correção do estado atual.Se o estado atual for uma resposta válida, verifique() retornará um zero.
		//Caso contrário, devolverá a quantidade de quadrados incorretos em cada um dos cinco testes: comparando estadoAtual com estado inicial,
		//verificando se o EstadoAtual não tem espaços em branco, verificando se cada linha no estadoAtual tem um número único que varia de um a nove,
		//verificando se cada coluna no estadoAtual tem um número único que varia de um a nove e, finalmente,
		//verificando se cada quadrado no estadoAtual tem um número único que varia de um a nove.
		public int verifique()
		{
			try
			{
				int incorreto = 0; // Número de quadrados incorretos.
				int partidas = 0; // Número de partidas em uma matriz

				//Verifique se os valores não - zero no estadoInicial são os mesmos números do estadoAtual.
				for (int col = 0; col < 9; col++)
				{
					for (int linha = 0; linha < 9; linha++)
					{
						if (estadoInicial[linha, col] != 0)
							if (estadoInicial[linha, col] != estadoAtual[linha, col])
								incorreto++;
					}
				}

				// Verifique se não há manchas em branco no sudoku.  Manchas em branco, neste programa,
				// são representado por zeros.Para isso, vamos simplesmente contar os zeros em todo o sudoku e adicioná-los ao 'incorreto'.
				for (int col = 0; col < 9; col++)
				{
					for (int linha = 0; linha < 9; linha++)
					{
						if (estadoAtual[col, linha] == 0)
							incorreto++;
					}
				}

				// Verifique se cada linha tem apenas números únicos e que cada número varia de um a nove.
				// Como isso é feito é que um terço para ciclos de loop através da matriz e conta as partidas.
				// Se o número de partidas for maior que um, em seguida, o número de partidas menos um é adicionado à variável incorreto,
				// e a variável de partida é zerada.
				for (int col = 0; col < 9; col++)
				{
					for (int linha = 0; linha < 9; linha++)
					{
						for (int i = 0; i < 9; i++)
						{
							if (estadoAtual[col, linha] == estadoAtual[col, i])
								partidas++;
						}

						if (partidas > 1)
						{
							incorreto += --partidas;
							partidas = 0;
						}
						else
						{
							partidas = 0;
						}
					}
				}

				// Verifique se cada coluna só tem números únicos e que cada número varia de Um a Nove.
				// Como isso é feito é que um terço para ciclos de loop através da matriz e conta as partidas.
				// Se o número de partidas é maior que Um, então o número de partidas menos Um é adicionado à variável incorreto, e a variável partidas é zerada.
				for (int linha = 0; linha < 9; linha++)
				{
					for (int col = 0; col < 9; col++)
					{
						for (int i = 0; i < 9; i++)
						{
							if (estadoAtual[col, linha] == estadoAtual[i, linha])
								partidas++;
						}

						if (partidas > 1)
						{
							incorreto += --partidas;
							partidas = 0;
						}
						else
						{
							partidas = 0;
						}
					}
				}

				//Verifique se cada quadrado tem apenas números únicos e que cada número varia de Um a Nove.
				//Primeiro, eu fiz nove matrizes 3x3 temporárias, então eu basicamente coloquei cada quadrado do sudoku em cada matriz,
				//então eu verifiquei a validade de cada quadrado, incrementando a variável incorreta por Um para cada duplicata em cada quadrado.


				//Divida sudoku em nove matrizes separadas, uma matriz para cada quadrado. Devido ao tamanho das matrizes temporárias,
				//tive que subtrair três ou seis dos índices de matriz para as matrizes temporárias para evitar uma matriz fora dos limites exceção.
				for (int linha = 0; linha < 9; linha++)
				{
					switch (linha)
					{
						case 0:
						case 1:
						case 2:
							for (int col = 0; col < 3; col++)
							{
								tempUm[linha, col] = estadoAtual[linha, col];
							}

							for (int col = 3; col < 6; col++)
							{
								tempDois[linha, col - 3] = estadoAtual[linha, col];
							}

							for (int col = 6; col < 9; col++)
							{
								tempTres[linha, col - 6] = estadoAtual[linha, col];
							}
							break;
						case 3:
						case 4:
						case 5:
							for (int col = 0; col < 3; col++)
							{
								tempQuatro[linha - 3, col] = estadoAtual[linha, col];
							}

							for (int col = 3; col < 6; col++)
							{
								tempCinco[linha - 3, col - 3] = estadoAtual[linha, col];
							}

							for (int col = 6; col < 9; col++)
							{
								tempSeis[linha - 3, col - 6] = estadoAtual[linha, col];
							}
							break;
						case 6:
						case 7:
						case 8:
							for (int col = 0; col < 3; col++)
							{
								tempSete[linha - 6, col] = estadoAtual[linha, col];
							}

							for (int col = 3; col < 6; col++)
							{
								tempOito[linha - 6, col - 3] = estadoAtual[linha, col];
							}

							for (int col = 6; col < 9; col++)
							{
								tempNove[linha - 6, col - 6] = estadoAtual[linha, col];
							}
							break;
						default:
							Console.WriteLine("Você não deveria estar aqui...");
							break;
					}
				}

				//Verifica se cada quadrado tem um inteiro único de um a nove.
				int partidasUm = 0;
				int partidasDois = 0;
				int partidasTres = 0;
				int partidasQuatro = 0;
				int partidasCinco = 0;
				int partidasSeis = 0;
				int partidasSete = 0;
				int partidasOito = 0;
				int partidasNove = 0;

				//Os primeiros Dois para loops fixam um número a ser comparado.
				for (int linha = 0; linha < 3; linha++)
				{
					for (int col = 0; col < 3; col++)
					{

						//Estes dois para ciclos de loops através do quadrado,
						//contando o número de partidas ao longo do caminho.
						for (int i = 0; i < 3; i++)
						{
							for (int j = 0; j < 3; j++)
							{
								if (tempUm[linha, col] == tempUm[i, j])
									partidasUm++;
								if (tempDois[linha, col] == tempDois[i, j])
									partidasDois++;
								if (tempTres[linha, col] == tempTres[i, j])
									partidasTres++;
								if (tempQuatro[linha, col] == tempQuatro[i, j])
									partidasQuatro++;
								if (tempCinco[linha, col] == tempCinco[i, j])
									partidasCinco++;
								if (tempSeis[linha, col] == tempSeis[i, j])
									partidasSeis++;
								if (tempSete[linha, col] == tempSete[i, j])
									partidasSete++;
								if (tempOito[linha, col] == tempOito[i, j])
									partidasOito++;
								if (tempNove[linha, col] == tempNove[i, j])
									partidasNove++;
							}
						}

						//Se houver mais de Um partidas, a variável partidas é decremente por Um(assim o número original não é contado)
						//e somada ao número de quadrados incorretos no sudoku.
						if (partidasUm > 1)
						{
							incorreto += --partidasUm;
							partidasUm = 0;
						}
						else
						{
							partidasUm = 0;
						}
						if (partidasDois > 1)
						{
							incorreto += --partidasDois;
							partidasDois = 0;
						}
						else
						{
							partidasDois = 0;
						}
						if (partidasTres > 1)
						{
							incorreto += --partidasTres;
							partidasTres = 0;
						}
						else
						{
							partidasTres = 0;
						}
						if (partidasQuatro > 1)
						{
							incorreto += --partidasQuatro;
							partidasQuatro = 0;
						}
						else
						{
							partidasQuatro = 0;
						}
						if (partidasCinco > 1)
						{
							incorreto += --partidasCinco;
							partidasCinco = 0;
						}
						else
						{
							partidasCinco = 0;
						}
						if (partidasSeis > 1)
						{
							incorreto += --partidasSeis;
							partidasSeis = 0;
						}
						else
						{
							partidasSeis = 0;
						}
						if (partidasSete > 1)
						{
							incorreto += --partidasSete;
							partidasSete = 0;
						}
						else
						{
							partidasSete = 0;
						}
						if (partidasOito > 1)
						{
							incorreto += --partidasOito;
							partidasOito = 0;
						}
						else
						{
							partidasOito = 0;
						}
						if (partidasNove > 1)
						{
							incorreto += --partidasNove;
							partidasNove = 0;
						}
						else
						{
							partidasNove = 0;
						}
					}
				}

				//Retorna o número de números incorretos na matriz.
				return incorreto;
			}
            catch(Exception)
            {
				return 1;
			}
		}

	}
}
