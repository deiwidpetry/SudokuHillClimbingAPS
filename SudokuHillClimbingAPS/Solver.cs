﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SudokuHillClimbing
{
	public abstract class Solver
	{
		protected int[,] estadoInicial;
		protected int[] numeros = new int[9];
		protected Sudoku temp;
		protected int erros;

		 
		//Esta função cria aleatoria de inteiros de um a nove.
		protected void popularNumeros()
		{
			Random randon = new Random();
			int elemento;

			for (int i = 0; i < 9; i++)
				numeros[i] = 0;

			for (int i = 0; i < 9; i++)
			{
				do
				{
					elemento = randon.Next(9) + 1;
				} while (existeNoArray(numeros, elemento));

				numeros[i] = elemento;
			}

		}

		protected void setarEstadoInicial(Sudoku solucao)
		{
			estadoInicial = solucao.pegarEstadoInicial();
		}

		protected bool existeNoArray(int[] array, int elemento)
		{
			for (int i = 0; i < 9; i++)
			{
				if (array[i] == elemento)
					return true;
			}
			return false;
		}

		protected void inicializarEstado(int[,] solucao)
		{
			//Objetivo: Se o valor atual do numeros[] for
			//em nenhum outro lugar na linha inicial Estado[i] e se existe um elemento em
			//estadoInicial[i] que é igual a zero, em seguida, atribuir um número do numeros.
			popularNumeros();
			for (int i = 0; i < 9; i++)
				for (int j = 0; j < 9; j++)
					for (int k = 0; k < 9; k++)
						switch (solucao[i,j])
						{
							case 0:
								if (!existeNoArray(linha(solucao, i), numeros[k]))
								{
									solucao[i,j] = numeros[k];
								}
								break;

							default:
								break;
						}
		}

		public int[] linha(int[,] solucao, int linha)
		{
			return new int[9] { solucao[linha, 0], solucao[linha, 1], solucao[linha, 2], solucao[linha, 3], solucao[linha, 4], solucao[linha, 5], solucao[linha, 6], solucao[linha, 7], solucao[linha, 8] };
		}

		private void Printar(int[,] quadro)
		{
			for (int i = 0; i < 9; i++)
			{
				for (int j = 0; j < 9; j++)
				{
					Console.Write(string.Format("{0} ", quadro[i, j]));
				}
				Console.Write(Environment.NewLine);
			}
			Console.ReadLine();
		}
	}
}
